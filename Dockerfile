FROM node:20.11-bullseye-slim

WORKDIR /usr/src/app

EXPOSE 3000

VOLUME /usr/src/app/data
