export default defineEventHandler((event) => {
  console.log(`[${event.node.res.statusCode}] - ${getRequestURL(event)}`);
});
